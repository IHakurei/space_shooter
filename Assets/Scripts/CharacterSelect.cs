﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSelect : MonoBehaviour {
    public GameObject boto1;
    public GameObject boto2;
    public GameObject boto3;
    public GameObject fons;
    public GameObject music;
    public GameObject music2;
    public GameObject alien1;
    public GameObject alien2;
    public GameObject alien3;
    public GameObject titulo;
    // Use this for initialization
    void Start () {
        boto1.SetActive(true);
        boto2.SetActive(true);
        boto3.SetActive(true);
        fons.SetActive(true);
        music.SetActive(true);
        music2.SetActive(false);
        alien1.SetActive(true);
        alien2.SetActive(true);
        alien3.SetActive(true);
        titulo.SetActive(true);
    }
	
    public void Tancar()
    {
        boto1.SetActive(false);
        boto2.SetActive(false);
        boto3.SetActive(false);
        fons.SetActive(false);
        music2.SetActive(true);
        music.SetActive(false);
        alien1.SetActive(false);
        alien2.SetActive(false);
        alien3.SetActive(false);
        titulo.SetActive(false);
    }


}
