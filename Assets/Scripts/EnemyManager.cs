﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour {

	public Transform launchPos;
	public float xVariation;
	public Transform creationPos;
	public float timeLaunch;
    public bool starto = false;

    public GameObject[] enemyPrefabs;
	private EnemyCache[] enemyCaches;
	private float currentTime;


	public static EnemyManager instance;

	// Use this for initialization
	void Awake () {
		instance = this;
		Vector3 creationPosEnemy = creationPos.position;
		enemyCaches = new EnemyCache[enemyPrefabs.Length];

		for (int i = 0; i < enemyPrefabs.Length; i++) {
			//Big Meteors;
			enemyCaches[i] = new EnemyCache (enemyPrefabs[i], creationPosEnemy, creationPos, 30);

			creationPosEnemy.y += 1;
		}
	}

	void Update(){
        if (starto==false)
            return;
		currentTime += Time.deltaTime;

		if (currentTime > timeLaunch) {
			enemyCaches [Random.Range(0,enemyCaches.Length)].GetEnemy ().LaunchEnemy (launchPos.position, new Vector2 (Random.Range(-5, 5), Random.Range(-5, -1)));
			currentTime -= timeLaunch;
		}
	}
    public void Starto()
    {
        starto = true;
    }

}
