﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shotproba : Bullet {

    public float timeFragmented = 1.0f;
    public float time2 = 2.0f;
    public Cartridge cartridge;
    private float currentTime = 0.0f;
    public Vector3 posini;
    public Vector3 posini2;
    void Awake()
    {
        cartridge = GameObject.Find("Weapons").GetComponent<Weapons>().cartridges[0];
    }

    protected new virtual void Update()
    {
        base.Update();
        if (shooting)
        {
            currentTime += Time.deltaTime;

            if (currentTime >= timeFragmented)
            {
                MultiShoot();
            }
            if (currentTime >= time2)
            {
                Mesbales();
            }
        }
    }
    private void MultiShoot()
    {
        cartridge.GetBullet().Shot(transform.position, -45f);
        cartridge.GetBullet().Shot(transform.position, 45f);



    }
    private void Mesbales()
    {

        cartridge.GetBullet().Shot(transform.position, -25f);
        cartridge.GetBullet().Shot(transform.position, 25f);
        Volta3();
        currentTime = 0.0f;

    }
    public void Volta3()
    {
        transform.position = posini2;
        shooting = false;
    }
    public void Volta2()
    {
        transform.position = posini;
        shooting = false;
    }
}
