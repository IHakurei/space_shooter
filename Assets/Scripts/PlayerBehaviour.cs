﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour {
	public float speed;
	private Vector2 axis;
	public Vector2 limits;
	public Propeller propeller;
	public ParticleSystem explosion;
	public GameObject graphics;
	private BoxCollider2D myCollider;
	public AudioSource audio;
    public SpriteRenderer sr;
    public Sprite nau1;
    public Sprite nau2;
    public Sprite nau3;
    private Vector2 localpos;
    private ShipInput ship;
    public bool starto = false;
    void Awake(){
		myCollider = GetComponent<BoxCollider2D> ();
        sr = GetComponent<SpriteRenderer>();
        localpos = transform.position;
        ship = new ShipInput();
	}
	// Update is called once per frame
	void Update () {
        if(starto==false)
            return;
		transform.Translate (axis * speed * Time.deltaTime);
        
		if (transform.position.x > limits.x) {
			transform.position = new Vector3 (limits.x, transform.position.y, transform.position.z);
		}else if (transform.position.x < -limits.x) {
			transform.position = new Vector3 (-limits.x, transform.position.y, transform.position.z);
		}

		if (transform.position.y > limits.y) {
			transform.position = new Vector3 (transform.position.x, limits.y, transform.position.z);
		}else if (transform.position.y < -limits.y) {
			transform.position = new Vector3 (transform.position.x, -limits.y, transform.position.z);
		}

		if (axis.y > 0) {
			propeller.RedFire ();
		} else if (axis.y < 0) {
			propeller.BlueFire ();
		} else {
			propeller.Stop ();
		}
	}

	public void SetAxis(Vector2 currentAxis){
		axis = currentAxis;
	}
	public void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Meteor" || other.tag =="enemy" || other.tag =="EnemyBullet") {
			Explode ();
            ship.Dead();
		}

	}
	private void Explode(){
		MyGamemanager.getInstance ().LoseLives ();
		graphics.SetActive (false);
		myCollider.enabled = false;
      
		explosion.Play ();
		audio.Play ();
		Invoke ("Reset", 2);
	}
	private void Reset(){
		graphics.SetActive (true);
		myCollider.enabled = true;
        transform.position = localpos;
        ship.Alive();
	}

    public void Starto()
    {
        starto = true;
    }
}
